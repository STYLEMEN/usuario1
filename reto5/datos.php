<?php

function conectarBBDD()
{
  //Abrir una conexión
  $mysqli = new mysqli("192.168.6.212", "reto", "Almi123", "reto5test");
  if($mysqli->connect_errno)
  {
    echo "Fallo en la conexión: ".$mysqli->connect_errno;
  }
  return $mysqli;
}

//REGISTRAR USUARIO
function insertarUsuario($usuario, $password, $email)
{
  $mysqli = conectarBBDD();

  $sql = "INSERT INTO user (usuario, password, email, id_permiso) VALUES (?, ?, ?, 3);";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la insert";
    }

    $bind = $sentencia->bind_param("sss", $usuario, $password, $email);
    if(!$bind)
    {
      echo "Error al asociar parámetros";
    }
    $resultado = $sentencia->execute();

    $mysqli->close();
    return $resultado;
  }

  function comprobarUsuario($user)
      {
        $mysqli = conectarBBDD();
        $sql = "SELECT * FROM user WHERE usuario = ?";

        $sentencia = $mysqli->prepare($sql);
        if(!$sentencia)
        {
          echo "Fallo al preparar la sentencia";
        }

        $asignar = $sentencia->bind_param("s", $user);
        if(!$asignar)
        {
          echo "Fallo en la asignación de parámetros";
        }

        $ejecucion = $sentencia->execute();
        if(!$ejecucion)
        {
          echo "Fallo al ejecutar";
        }

        $iduser = -1;
        $usuario = "";
        $contrasenya = "";
        $email = "";
        $admin = -1;
        $vincular = $sentencia->bind_result($iduser, $usuario, $contrasenya, $email, $admin);
        if(!$vincular)
        {
          echo "Fallo al vincular";
        }

        $usuarioResultado = array();

        if($sentencia->fetch())
        {
          $usuarioResultado = array(
            'id_usuario' => $iduser,
            'usuario' => $usuario,
            'contrasenya' => $contrasenya,
            'email' => $email,
            'admin' => $admin
          );
        }
        $mysqli->close();
        return $usuarioResultado;
      }

    function login($user, $password)
    {
      $mysqli = conectarBBDD();
      $sql = "SELECT * FROM user WHERE usuario = ? AND password = ?";

      $sentencia = $mysqli->prepare($sql);
      if(!$sentencia)
      {
        echo "Fallo al preparar la sentencia";
      }

      $asignar = $sentencia->bind_param("ss", $user, $password);
      if(!$asignar)
      {
        echo "Fallo en la asignación de parámetros";
      }

      $ejecucion = $sentencia->execute();
      if(!$ejecucion)
      {
        echo "Fallo al ejecutar";
      }

      $iduser = -1;
      $usuario = "";
      $contrasenya = "";
      $email = "";
      $admin = -1;
      $vincular = $sentencia->bind_result($iduser, $usuario, $contrasenya, $email, $admin);
      if(!$vincular)
      {
        echo "Fallo al vincular";
      }

      $usuarioResultado = array();

      if($sentencia->fetch())
      {
        $usuarioResultado = array(
          'id_user' => $iduser,
          'usuario' => $usuario,
          'contrasenya' => $contrasenya,
          'email' => $email,
          'admin' => $admin
        );
      }
      $mysqli->close();
      return $usuarioResultado;
    }

    function detallesUsuario($iduser)
        {
          $mysqli = conectarBBDD();
          $sql = "SELECT * FROM user WHERE id_usuario = ?";

          $sentencia = $mysqli->prepare($sql);
          if(!$sentencia)
          {
            echo "Fallo al preparar la sentencia";
          }

          $asignar = $sentencia->bind_param("i", $iduser);
          if(!$asignar)
          {
            echo "Fallo en la asignación de parámetros";
          }

          $ejecucion = $sentencia->execute();
          if(!$ejecucion)
          {
            echo "Fallo al ejecutar";
          }

          $iduser = -1;
          $usuario = "";
          $email = "";
          $contrasenya = "";
          $admin = -1;
          $vincular = $sentencia->bind_result($iduser, $usuario, $email, $contrasenya, $admin);
          if(!$vincular)
          {
            echo "Fallo al vincular";
          }

          $usuarioResultado = array();

          if($sentencia->fetch())
          {
            $usuarioResultado = array(
              'id_usuario' => $iduser,
              'usuario' => $usuario,
              'email' => $email,
              'contrasenya' => $contrasenya,
              'admin' => $admin
            );
          }
          $mysqli->close();
          return $usuarioResultado;
        }

    function modificarUsuario($idusuario, $usuario, $contrasena, $email)
    {
      $mysqli = conectarBBDD();

      $sql = "UPDATE user SET usuario=?,  password=?, email=? WHERE id_usuario=?";
      $sentencia = $mysqli->prepare($sql);
      if(!$sentencia)
      {
        echo "Fallo al preparar la insert";
      }

      $bind = $sentencia->bind_param("sssi", $usuario, $contrasena, $email, $idusuario);
      if(!$bind)
      {
        echo "Error al asociar parámetros";
      }
      $resultado = $sentencia->execute();

      $mysqli->close();
      return $resultado;
    }

    function usuarios()
        {
          $mysqli = conectarBBDD();
          $sql = "SELECT * FROM user WHERE id_permiso is null";

          $sentencia = $mysqli->prepare($sql);
          if(!$sentencia)
          {
            echo "Fallo al preparar la sentencia";
          }
          $ejecucion = $sentencia->execute();
          if(!$ejecucion)
          {
            echo "Fallo al ejecutar";
          }

          $iduser = -1;
          $usuario = "";
          $email = "";
          $contrasenya = "";
          $admin = -1;
          $vincular = $sentencia->bind_result($iduser, $usuario, $email, $contrasenya, $admin);
          if(!$vincular)
          {
            echo "Fallo al vincular";
          }

          $usuarios = array();

          while($sentencia->fetch())
          {
            $usuario = array(
              'id_usuario' => $iduser,
              'usuario' => $usuario,
              'email' => $email,
              'contrasenya' => $contrasenya,
              'admin' => $admin
            );
            $usuarios[]= $usuario;
          }
          $mysqli->close();
          return $usuarios;
        }

        function moderadores()
            {
              $mysqli = conectarBBDD();
              $sql = "SELECT * FROM user WHERE id_permiso=2";

              $sentencia = $mysqli->prepare($sql);
              if(!$sentencia)
              {
                echo "Fallo al preparar la sentencia";
              }
              $ejecucion = $sentencia->execute();
              if(!$ejecucion)
              {
                echo "Fallo al ejecutar";
              }

              $iduser = -1;
              $usuario = "";
              $email = "";
              $contrasenya = "";
              $admin = -1;
              $vincular = $sentencia->bind_result($iduser, $usuario, $email, $contrasenya, $admin);
              if(!$vincular)
              {
                echo "Fallo al vincular";
              }

              $usuarios = array();

              while($sentencia->fetch())
              {
                $usuario = array(
                  'id_usuario' => $iduser,
                  'usuario' => $usuario,
                  'email' => $email,
                  'contrasenya' => $contrasenya,
                  'admin' => $admin
                );
                $usuarios[]= $usuario;
              }
              $mysqli->close();
              return $usuarios;
            }
            function permiso($idusuario)
            {
              var_dump($idusuario);
              $mysqli = conectarBBDD();

              $sql = "UPDATE user SET id_permiso=2 WHERE id_usuario=?";
              $sentencia = $mysqli->prepare($sql);
              if(!$sentencia)
              {
                echo "Fallo al preparar la insert";
              }

              $bind = $sentencia->bind_param("i", $idusuario);
              if(!$bind)
              {
                echo "Error al asociar parámetros";
              }
              $resultado = $sentencia->execute();

              $mysqli->close();
              return $resultado;
            }


      function borrarUsuario($idusuario)
      {
        $mysqli = conectarBBDD();

        $sql = "DELETE FROM user WHERE id_usuario=?";
        $sentencia = $mysqli->prepare($sql);
        if(!$sentencia)
        {
          echo "Fallo al preparar la insert";
        }

        $bind = $sentencia->bind_param("i", $idusuario);
        if(!$bind)
        {
          echo "Error al asociar parámetros";
        }
        $resultado = $sentencia->execute();

        $mysqli->close();
        return $resultado;
      }
      function permisos()
          {
            $mysqli = conectarBBDD();
            $sql = "SELECT * FROM permisos";

            $sentencia = $mysqli->prepare($sql);
            if(!$sentencia)
            {
              echo "Fallo al preparar la sentencia";
            }
            $ejecucion = $sentencia->execute();
            if(!$ejecucion)
            {
              echo "Fallo al ejecutar";
            }

            $id = -1;
            $nombre = "";
            $vincular = $sentencia->bind_result($id, $nombre);
            if(!$vincular)
            {
              echo "Fallo al vincular";
            }

            $permisos = array();

            while($sentencia->fetch())
            {
              $permiso = array(
                'id_permiso' => $id,
                'nombre' => $nombre
              );
              $permisos[]= $permiso;
            }
            $mysqli->close();
            return $permisos;
          }


          function usuariosbyIdPermiso($idpermiso)
              {
                $mysqli = conectarBBDD();
                $sql = "SELECT * FROM user WHERE id_permiso = ?";

                $sentencia = $mysqli->prepare($sql);
                if(!$sentencia)
                {
                  echo "Fallo al preparar la sentencia";
                }

                $asignar = $sentencia->bind_param("i", $idpermiso);
                if(!$asignar)
                {
                  echo "Fallo en la asignación de parámetros";
                }

                $ejecucion = $sentencia->execute();
                if(!$ejecucion)
                {
                  echo "Fallo al ejecutar";
                }

                $iduser = -1;
                $usuario = "";
                $email = "";
                $contrasenya = "";
                $admin = -1;
                $vincular = $sentencia->bind_result($iduser, $usuario, $email, $contrasenya, $admin);
                if(!$vincular)
                {
                  echo "Fallo al vincular";
                }

                $usuariosResultado = array();

                while($sentencia->fetch())
                {
                  $usuarioResultado = array(
                    'id_usuario' => $iduser,
                    'usuario' => $usuario,
                    'email' => $email,
                    'contrasenya' => $contrasenya,
                    'admin' => $admin
                  );
                  $usuariosResultado[] = $usuarioResultado;
                }
                $mysqli->close();
                return $usuariosResultado;
              }
              function quitarpermiso($idusuario)
              {
                var_dump($idusuario);
                $mysqli = conectarBBDD();

                $sql = "UPDATE user SET id_permiso=3 WHERE id_usuario=?";
                $sentencia = $mysqli->prepare($sql);
                if(!$sentencia)
                {
                  echo "Fallo al preparar la insert";
                }

                $bind = $sentencia->bind_param("i", $idusuario);
                if(!$bind)
                {
                  echo "Error al asociar parámetros";
                }
                $resultado = $sentencia->execute();

                $mysqli->close();
                return $resultado;
              }

              function borrarPartida($idusuario)
              {
                $mysqli = conectarBBDD();

                $sql = "DELETE FROM partida WHERE id_usuario=?";
                $sentencia = $mysqli->prepare($sql);
                if(!$sentencia)
                {
                  echo "Fallo al preparar la insert";
                }

                $bind = $sentencia->bind_param("i", $idusuario);
                if(!$bind)
                {
                  echo "Error al asociar parámetros";
                }
                $resultado = $sentencia->execute();

                $mysqli->close();
                return $resultado;
              }

?>
