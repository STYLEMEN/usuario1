<?php

if(isset($_SERVER['HTTP_ORIGIN']))
{
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Max-Age: 86400");
}

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
{
  if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
    header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS");
  if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
    header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
  exit(0);
}

header('Content-Type: application/JSON');

$function = $_POST['function'];
include 'datos.php';

if($function == 'comprobarUsuario')
{
  $usuario = comprobarUsuario($_POST['usuario']);
  $usuarioJson = json_encode($usuario, JSON_UNESCAPED_UNICODE);
  echo $usuarioJson;
} else if ($function == 'login')
{
  $usuario = $_POST['usuario'];
  $contasena = $_POST['contrasena'];

  $login = login($usuario, $contasena);
  if($login == true)
  {
    $loginJson = true;
  }
  else {
    $loginJson = false;
  }
  //$loginJson = json_encode($login, JSON_UNESCAPED_UNICODE);
  echo $loginJson;
}else if($function == 'usuariosbyIdPermiso')
{
  $usuario = usuariosbyIdPermiso($_POST['id_permiso']);
  $usuarioJson = json_encode($usuario, JSON_UNESCAPED_UNICODE);
  echo $usuarioJson;
}
?>
