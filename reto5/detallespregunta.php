<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Reto 5</title>
    <link rel="stylesheet" type="text/css" href="css/detallespregunta.css">
    <link rel="stylesheet" type="text/css" href="css/comun.css">
    <?php
      session_start();
      include("datos.php");
    ?>
  </head>
  <body>
    <div id="encabezado">
      <h1>STYLEMEN</h1>
      <div id="menu">
        <ul>
          <li>
            <a href="index.php" class="enlaceInicio">Inicio</a>
          </li>
          <?php

          if (isset( $_SESSION["user"] ) == false){
          echo"<li>";
            echo"<a href='login.php'class='enclaceLogin'>Inicio sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='registro.php' class='enlaceRegistro'>Registar</a>";
          echo "</li>";
        }else {
          if( $_SESSION['admin'] == 1)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
            echo "<li>";
              echo"<a href='filtroUsuario.php'>Usuarios</a>";
            echo "</li>";
          }
          if( $_SESSION['admin'] == 2)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
          }
          echo"<li>";
            echo"<a href='crearpregunta.php'class='enclaceLogin'>Crear Pregunta</a>";
          echo"</li>";
          echo "<li><a href='validar.php'>Validar Preguntas</a></li>";
          echo"<li>";
            echo"<a href='cerrarSesion.php'class='enclaceLogin'>Cerrar Sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='usuario.php?id_usuario=".$_SESSION['id_user']."' class='enlaceRegistro'>".$_SESSION['user']."</a>";
          echo "</li>";


        }
          ?>
        </ul>
      </div>
    </div>
    <div id="cuerpo">
      <?php
      $id = $_GET['id'];
      //var_dump($id);
      $dataJson = json_decode( file_get_contents( "http://192.168.6.212:8080/api/preguntas/".$id ), true);


      for($i = 0; $i < count($dataJson['data']['pregunta']); $i++)
      {
        echo "<h2 id='enunciado'>".$dataJson['data']['pregunta'][$i]['enunciado']."</h2>";
        echo "<img id='imagen' src='img".$dataJson['data']['imagen']."'>";
        echo "<div id='respuestas'>";
          echo "<div id='respuestaA'>";
            echo "<h4 class='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['a'][1]."'>".$dataJson['data']['pregunta'][$i]['respuestas'][0]['a'][0]."</h4>";
            echo "</div>";
            echo "<div id='respuestaB'>";
              echo "<h4 class='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['b'][1]."'>".$dataJson['data']['pregunta'][$i]['respuestas'][0]['b'][0]."</h4>";
            echo "</div>";
            echo "<div id='respuestaC'>";
              echo "<h4 class='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['c'][1]."'>".$dataJson['data']['pregunta'][$i]['respuestas'][0]['c'][0]."</h4>";
            echo "</div>";
            echo "<div id='respuestaD'>";
              echo "<h4 class='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['d'][1]."'>".$dataJson['data']['pregunta'][$i]['respuestas'][0]['d'][0]."</h4>";
            echo "</div>";
        echo "</div>";
      }
       ?>
       <input type="button"class="boton" id="botonRespuesta" value="Respuesta">
       <input type="button" class="boton" id="bontonSinRespuesta" value="Sin respuesta">

       <?php
       for($i = 0; $i < count($dataJson['data']['pregunta']); $i++)
       {
         echo "<div id='explicacion'>";
          echo "<h3>Explicacion</h3>";
           echo "<p>".$dataJson['data']['pregunta'][$i]['explicacion']."</p>";
          echo "</div>";
       }
        ?>

    </div>
    <div id="pie">
      <img src="" alt="">

    </div>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/detallespregunta.js"></script>
    </body>
    </html>
