<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Reto 5</title>
    <link rel="stylesheet" type="text/css" href="css/usuario.css">
    <link rel="stylesheet" type="text/css" href="css/comun.css">
    <?php
      session_start();
      include("datos.php");
    ?>
  </head>
  <body>
    <div id="encabezado">
      <h1>STYLEMEN</h1>
      <div id="menu">
        <ul>
          <li>
            <a href="index.php" class="enlaceInicio">Inicio</a>
          </li>
          <?php

          if (isset( $_SESSION["user"] ) == false){
          echo"<li>";
            echo"<a href='login.php'class='enclaceLogin'>Inicio sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='registro.php' class='enlaceRegistro'>Registar</a>";
          echo "</li>";
        }else {
          if( $_SESSION['admin'] == 1)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
            echo "<li>";
              echo"<a href='filtroUsuario.php'>Usuarios</a>";
            echo "</li>";
          }
          if( $_SESSION['admin'] == 2)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
          }
          echo"<li>";
            echo"<a href='crearpregunta.php'class='enclaceLogin'>Crear Pregunta</a>";
          echo"</li>";
          echo "<li><a href='validar.php'>Validar Preguntas</a></li>";
          echo"<li>";
            echo"<a href='cerrarSesion.php'class='enclaceLogin'>Cerrar Sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='usuario.php?id_usuario=".$_SESSION['id_user']."' class='enlaceRegistro'>".$_SESSION['user']."</a>";
          echo "</li>";


        }
          ?>
        </ul>
      </div>
    </div>
    <div id="cuerpo">
      <?php
      $usuario = detallesUsuario($_GET["id_usuario"]);
      if( $_SESSION['admin'] != 1)
      {
      echo "<a href='borrarUsuariopersonal.php?id_usuario=".$_SESSION['id_user']."'><input type='button' name='' value='Eliminar cuenta' class='borrar'></input></a>";
    }
      echo "<form id='formulario' action='modificarUsuario.php' method='post'>";

      //var_dump($usuario);

      echo "<input type='hidden' id='id_usuario' name='id_usuario' value='".$usuario['id_usuario']."'/>";
      echo "<br>";
      echo "<label>Usuario:</label>";
      echo "<input id='usuario' type='text' name='usuario' value='".$usuario['usuario']."' readOnly = 'true'/>";
      echo"<div id='resultado'>";
      echo"</div>";
      echo "<label>Contraseña:</label>";
      echo "<input id='contrasena' name='contrasena' type='password' value='".$usuario['contrasenya']."' readOnly = 'true'/>";
      ?>
      <input type='checkbox' id='mostrar_contrasena'/>
      <?php
      echo "<div id='mensajeContraseña'></div>";
      echo "<label>Correo Electronico:</label>";
      echo "<input id='email' name='email' type='text' value='".$usuario['email']."' readOnly = 'true'/>";
      echo "<div id='mensajeCorreo'></div>";
      ?>
      <br>
      <input id="enviar" type="submit" value="Guardar Cambios">
      <input id="modificar" type="button" value="Modificar">
      <input id="cancelar" type="button" value="Cancelar">
      </form>

      <br>
    </div>
    <div id="pie">
      <footer>

      </footer>
    </div>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/usuario.js"></script>
  </body>
</html>
