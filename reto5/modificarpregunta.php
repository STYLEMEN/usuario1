<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Reto 5</title>
    <link rel="stylesheet" type="text/css" href="css/modificarpreguntas.css">
    <link rel="stylesheet" type="text/css" href="css/comun.css">
    <?php
      session_start();
      include("datos.php");
    ?>
  </head>
  <body>
    <div id="encabezado">
      <h1>STYLEMEN</h1>
      <div id="menu">
        <ul>
          <li>
            <a href="index.php" class="enlaceInicio">Inicio</a>
          </li>
          <?php

          if (isset( $_SESSION["user"] ) == false){
          echo"<li>";
            echo"<a href='login.php'class='enclaceLogin'>Inicio sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='registro.php' class='enlaceRegistro'>Registar</a>";
          echo "</li>";
        }else {
          if( $_SESSION['admin'] == 1)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
            echo "<li>";
              echo"<a href='filtroUsuario.php'>Usuarios</a>";
            echo "</li>";
          }
          if( $_SESSION['admin'] == 2)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
          }
          echo"<li>";
            echo"<a href='crearpregunta.php'class='enclaceLogin'>Crear Pregunta</a>";
          echo"</li>";
          echo "<li><a href='validar.php'>Validar Preguntas</a></li>";
          echo"<li>";
            echo"<a href='cerrarSesion.php'class='enclaceLogin'>Cerrar Sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='usuario.php?id_usuario=".$_SESSION['id_user']."' class='enlaceRegistro'>".$_SESSION['user']."</a>";
          echo "</li>";


        }
          ?>
        </ul>
      </div>
    </div>
    <div id="cuerpo">
      <?php
      $id = $_GET['id'];
      //var_dump($id);
      $dataJson = json_decode( file_get_contents( "http://192.168.6.212:8080/api/preguntas/".$id ), true);

      for($i = 0; $i < count($dataJson['data']['pregunta']); $i++)
      {
        echo "<form id='formulario' action='mongoFOL.php'>";
          echo "<br>";
          echo "<label>Pregunta</label>";
          echo "<br>";
          echo "<input id='id' type='hidden' value='".$id."'>";
          echo "<input id='pregunta' type='text' value='".$dataJson['data']['pregunta'][$i]['enunciado']."'>";
          echo "<br>";
          echo "<br>";
          echo "<label>Respuesta 1</label>";
          echo "<br>";
          echo "<input id='respuesta1' type='text' value='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['a'][0]."'>";
          echo "<input type='checkbox'id='validacion1' class='validacion'value='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['a'][1]."'>";
          echo "</input>";
          echo "<br>";
          echo "<br>";
          echo "<label>Respuesta 2</label>";
          echo "<br>";
          echo "<input id='respuesta2' type='text' value='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['b'][0]."'>";
          echo "<input type='checkbox'id='validacion2' class='validacion' value='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['b'][1]."'>";
          echo "</input>";
          echo "<br>";
          echo "<br>";
          echo "<label>Respuesta 3</label>";
          echo "<br>";
          echo "<input id='respuesta3' type='text' value='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['c'][0]."'>";
          echo "<input type='checkbox'id='validacion3' class='validacion'value='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['c'][1]."'>";
          echo "</input>";
          echo "<br>";
          echo "<br>";
          echo "<label>Respuesta 4</label>";
          echo "<br>";
          echo "<input id='respuesta4' type='text' value='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['d'][0]."'>";
          echo "<input type='checkbox'id='validacion4' class='validacion' value='".$dataJson['data']['pregunta'][$i]['respuestas'][0]['d'][1]."'>";
          echo "</input>";
          echo "<br>";
          echo "<br>";
          echo "<label>Explicacion</label>";
          echo "<br>";
          echo "<input id='explicacion' type='text' value='".$dataJson['data']['pregunta'][$i]['explicacion']."'>";
          echo "<br>";
          echo "<br>";
          echo "<label>Categoria</label>";
          echo "<select id='categoria'>";
          echo "<option></option>";
          echo "<option>FOL</option>";
          echo "<option>Lenguaje</option>";
          echo "<option>JAVA</option>";
          echo "<option>Ingles</option>";
          echo "</select>";
          echo "<br>";
          echo "<div id='mensajecategoria'>";
          echo "<p>Selecione una categoria</p>";
          echo "</div>";
          echo "<br>";
          echo"<input id='modificar' type='submit' value='Guardar Cambios'>";
        echo "</form>";
      }
      ?>

      <br>
    </div>
    <div id="pie">
    </div>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/modificarpregunta.js"></script>
  </body>
</html>
