$(document).ready(function()
{
//BOTONES--------------------------------------------------------------------------------------------------------------------
  var usuario = $('#usuario').val();
  var contrasena = $('#contrasena').val();
  var email = $('#email').val();

  var error;

  $('#modificar').click(function()
  {
    $('#cancelar').show();
    $('#enviar').show();
    $('#modificar').hide();

    document.getElementById("usuario").readOnly = false;
    document.getElementById("contrasena").readOnly = false;
    document.getElementById("email").readOnly = false;
  });

  $('#cancelar').click(function()
  {
    $('#cancelar').hide();
    $('#enviar').hide();
    $('#modificar').show();

    document.getElementById("usuario").readOnly = true;
    document.getElementById("contrasena").readOnly = true;
    document.getElementById("email").readOnly = true;

    $('#email').val(email);
    $('#contrasena').val(contrasena);
    $('#usuario').val(usuario);
    $('#usuario').css("color", "black");

    $('#mensajeCorreo').hide();
    $('#resultado').hide();
    $('#mensajeContraseña').hide();

    $('#contrasena').css("background-color", "white");
    $("#email").css("border", " 1px solid Empty");
    $("#usuario").css("border", " 1px solid Empty");
    $("#contrasena").css("border", " 1px solid Empty");
  });

  $('#mostrar_contrasena').click(function ()
  {
    if ($('#mostrar_contrasena').is(':checked'))
    {
      $('#contrasena').attr("type", "text");
    } else
    {
      $('#contrasena').attr("type", "password");
    }
  });

//USUARIO---------------------------------------------------------------------------------------------------------------
  $("#usuario").on("change textInput input", function()
  {
    $('#usuario').css("color", "black");
    //$('#mensajeContraseña').val("");
    $('#resultado').html("");
    $('#resultado').hide();

    $("#usuario").css("border", " 1px solid Empty");

    var usuario1 = $(this).val();

    if(usuario1 != usuario){
    var parametros = {function: 'comprobarUsuario', usuario: usuario1};

    $.ajax(
      {
        data:parametros,
        url:"http://192.168.6.215/servicios.php",
        type:"post",
        success:function(response)
        {
          var usuariosArray = $.parseJSON(response);

          if (usuariosArray.length == 0) {
          error = 0;
          }else {
            error = 1;
          }
        },
        error:function(err)
        {
          console.log(err);
        }
    });
    console.log(error);
  }
  });


  $('#usuario').blur(function()
  {
    var mayuscula;
    var usuario1 = $(this).val();

    $('#usuario').css("color", "black");
    $('#resultado').html("");

    for(var i = 0; i <= usuario.length; i++){
      if(usuario1.charCodeAt(i) >= 65 && usuario1.charCodeAt(i) <= 90)
      {
        mayuscula = 1;
      }
    }
    if (mayuscula == 1){
      $('#usuario').css("color", "red");
      $('#resultado').append("<p>No debe contener mayusculas</p>");
      $('#resultado').show();
      error = 1;
    }
    else
    {
      if(usuario1 != usuario){
      var parametros = {function: 'comprobarUsuario', usuario: usuario1};

      $.ajax(
        {
          data:parametros,
          url:"http://192.168.6.215/servicios.php",
          type:"post",
          success:function(response)
          {
            var usuariosArray = $.parseJSON(response);

            if (usuariosArray.length == 0) {
              $('#usuario').css("color", "green");
            }else {
              $('#usuario').css("color", "red");
              $('#resultado').append("<p>Usuario ya existe</p>");
              $('#resultado').show();
            }
          },
          error:function(err)
          {
            console.log(err);
          }
        });
      console.log(error);
      }
    }
  });

//CONTRASEÑA--------------------------------------------------------------------------------------------------

$('#contrasena').blur(function()
{
  var contrasena = $(this).val();

  $(this).css("background-color", "White");
  $('#mensajeContraseña').html("");
  for(var i = 0; i <= contrasena.length; i++)
  {
    var cantidadCaracteres = i;
  }
  if(cantidadCaracteres <= 3)
  {

    if(cantidadCaracteres > 0)
    {
      $(this).css("background-color", "#f4050591");
    }
    if(cantidadCaracteres == 0)
    {
      $(this).css("background-color", "white");
    }
    $('#mensajeContraseña').append("Al menos debe tener 4 caracteres");
    $('#mensajeContraseña').fadeIn(1000);
  }
});


$("#contrasena").on("change textInput input", function()
{
  var contrasena = $(this).val();
  $("#contrasena").css("border", " 1px solid white");
  for(var i = 0; i <= contrasena.length; i++)
  {
    var cantidadCaracteres = i;
  }
  if(cantidadCaracteres <= 3)
  {
  }else{

    $('#mensajeContraseña').html("");
    $('#mensajeContraseña').fadeout(750);
    $('#mensajeContraseña').hide();

  }
});

//CORREO Electronico-------------------------------------------------------------------------------------------------



$('#email').blur(function()
{
  var email = $('#email').val();
  var indice = email.indexOf('@');

  console.log(email);
  if(indice <= 0)
  {
    $("#email").css("border", " 1px solid #f4050591");
    $('#mensajeCorreo').html("<p>El email debe contener una @.</p>");
    $('#mensajeCorreo').fadeIn(750);

  }
  else {
    $('#mensajeCorreo').hide();
    $("#email").css("border", " 1px solid Empty");
  }
  console.log(error);
});

$("#email").on("change textInput input", function()
{
  $("#email").css("border", " 1px solid white");
});

//SUBMIT-------------------------------------------------------------------------------------------------

  $('#formulario').submit(function(event)
  {
    var email = $("#email").val();
    var indice = email.indexOf('@');

    var pass1 = $("#contrasena").val();


    for(var i = 0; i <= pass1.length; i++)
    {
      var cantidadCaracteres = i;
    }
    if(cantidadCaracteres <= 3)
    {
      event.preventDefault();
      $("#contrasena").css("border", " 1px solid red");
    }

    if(indice <= 0)
    {
      event.preventDefault();
      $("#email").css("border", " 1px solid red");
    }
    if (error == 1)
    {
      event.preventDefault();
      $("#usuario").css("border", " 1px solid red");
    }
    var user = $('#usuario').val();

    for(var i = 0; i <= user.length; i++)
    {
      if(user.charCodeAt(i) >=65 && user.charCodeAt(i) <= 90)
      {
      event.preventDefault();
      $("#usuario").css("border", " 1px solid red");
      }
      var cantidadUsuario = i;
    }
    if (cantidadUsuario == 0) {
      event.preventDefault();
      $("#usuario").css("border", " 1px solid red");
    }
  });
});
