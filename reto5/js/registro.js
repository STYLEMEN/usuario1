$(document).ready(function()
{
  var error;

//USUARIO----------------------------------------------------------------------------------------------------------------------------------------------------
  $('#usuario').blur(function()
  {
    var mayuscula;
    var usuario = $(this).val();

    $('#usuario').css("color", "black");
    $('#resultado').html("");

    for(var i = 0; i <= usuario.length; i++){
      if(usuario.charCodeAt(i) >= 65 && usuario.charCodeAt(i) <= 90)
      {
        mayuscula = 1;
      }
    }
    if (mayuscula == 1){
      $('#usuario').css("color", "red");
      $('#resultado').append("<p>No debe contener mayusculas</p>");
      $('#resultado').show();
      error = 1;
    }
    else
    {
      var parametros = {function: 'comprobarUsuario', usuario: usuario};

      $.ajax(
        {
          data:parametros,
          url:"http://192.168.6.215/servicios.php",
          type:"post",
          success:function(response)
          {
            var usuariosArray = $.parseJSON(response);
            console.log(usuariosArray);
            if (usuariosArray.length == 0) {
              $('#usuario').css("color", "green");
            }else {
              $('#usuario').css("color", "red");
              $('#resultado').append("<p>Usuario ya existe</p>");
              $('#resultado').show();
            }
          },
          error:function(err)
          {
            console.log(err);
          }
      });
    }
console.log(error);
  });

  $("#usuario").on("change textInput input", function()
  {
    $('#usuario').css("color", "black");
    $('#mensajeContraseña').val("");
    $('#resultado').html("");
    $('#resultado').hide();

    $("#usuario").css("border", " 1px solid white");

    var usuario = $(this).val();

    var parametros = {function: 'comprobarUsuario', usuario: usuario};

    $.ajax(
      {
        data:parametros,
        url:"http://192.168.6.215/servicios.php",
        type:"post",
        success:function(response)
        {
          var usuariosArray = $.parseJSON(response);

          if (usuariosArray.length == 0) {
          error = 0;
          }else {
            error = 1;
          }
        },
        error:function(err)
        {
          console.log(err);
        }
    });
    console.log(error);
  });

//CONTRASEÑAS-----------------------------------------------------------------------------------------------------------------------------------------
  $('#contrasena').blur(function()
  {
    var contrasena = $(this).val();

    $(this).css("background-color", "White");

    $('#repetirContrasena').html("");
    $('#mensajeContraseña').html("");

    $('#repetirContrasena').css("background-color", "white");

    //$('#contasenaRepetir').fadeOut(750);

    for(var i = 0; i <= contrasena.length; i++)
    {
      var cantidadCaracteres = i;
    }
    if(cantidadCaracteres <= 3)
    {

      if(cantidadCaracteres > 0)
      {
        $(this).css("background-color", "#f4050591");
      }
      if(cantidadCaracteres == 0)
      {
        $(this).css("background-color", "white");
        $('#repetirContrasena').val("");
      }
      $('#mensajeContraseña').append("Al menos debe tener 4 caracteres");
      $('#mensajeContraseña').fadeIn(1000);

    } else {
      //$('#contasenaRepetir').fadeIn(1000);
      //$('#mensajeContraseña').hide();
      $('#repetirContrasena').html("");
    }
    console.log(error);
  });

  $("#contrasena").on("change textInput input", function()
  {
    var contrasena = $(this).val();
    $("#contrasena").css("border", " 1px solid white");
    for(var i = 0; i <= contrasena.length; i++)
    {
      var cantidadCaracteres = i;
    }
    if(cantidadCaracteres <= 3)
    {
      $('#contasenaRepetir').fadeOut(750);
    }else{
      $('#contasenaRepetir').fadeIn(750);
      $('#mensajeContraseña').html("");
      $('#mensajeContraseña').fadeout(750);
      $('#mensajeContraseña').hide();

    }
  });

  $('#mostrar_contrasena').click(function ()
  {
    if ($('#mostrar_contrasena').is(':checked'))
    {
      $('#contrasena').attr("type", "text");
    } else
    {
      $('#contrasena').attr("type", "password");
    }
  });

  $('#repetirContrasena').blur(function()
  {
    var pass1 = $('#contrasena').val();
    var pass2 = $('#repetirContrasena').val();

    for(var i = 0; i <= pass2.length; i++)
    {
      var cantidadCaracteres = i;
    }
    if(cantidadCaracteres == 0)
    {
      $('#repetirContrasena').css("border", " 1px solid #f6f6f6");
      $('#repetirContrasena').css("background-color", "white");
    }
    else
    {
      if(pass1 != pass2)
      {
        $('#repetirContrasena').css("background-color", "#f4050591");
        error = 1;
      }
      else {
        $('#repetirContrasena').css("border", " 1px solid green");
      }
    }
    console.log(error);
  });

  $("#repetirContrasena").on("change textInput input", function()
  {
    $('#repetirContrasena').css("background-color", "white");
    $('#repetirContrasena').css("border", " 1px solid #f6f6f6");
    $("#repetirContrasena").css("border", " 1px solid white");
  });

//CORREO ELECTRONICO--------------------------------------------------------------------------------------------------------------------------
  $('#email').blur(function()
  {
    var email = $('#email').val();
    var indice = email.indexOf('@');

    console.log(email);
    if(indice <= 0)
    {
      $("#email").css("border", " 1px solid #f4050591");
      $('#mensajeCorreo').html("<p>El email debe contener una @.</p>");
      $('#mensajeCorreo').fadeIn(750);

    }
    else {
      $('#mensajeCorreo').hide();
      $("#email").css("border", " 1px solid green");
    }
    console.log(error);
  });

  $("#email").on("change textInput input", function()
  {
    $("#email").css("border", " 1px solid white");
  });

//SUBMIT---------------------------------------------------------------------------------------------------------------------------------------

  $('#formularioRegistro').submit(function(event)
  {
    var email = $("#email").val();
    var indice = email.indexOf('@');

    var pass1 = $("#contrasena").val();
    var pass2 = $("#repetirContrasena").val();


    for(var i = 0; i <= pass1.length; i++)
    {
      var cantidadCaracteres = i;
    }
    if(cantidadCaracteres <= 3)
    {
      event.preventDefault();
      $("#contrasena").css("border", " 1px solid red");
    }

    if(pass1 != pass2)
    {
      event.preventDefault();
      $("#repetirContrasena").css("border", " 1px solid red");

    }
    if(indice <= 0)
    {
      event.preventDefault();
      $("#email").css("border", " 1px solid red");
    }
    if (error == 1)
    {
      event.preventDefault();
      $("#usuario").css("border", " 1px solid red");
    }

    var user = $('#usuario').val();

    for(var i = 0; i <= user.length; i++)
    {
      if(user.charCodeAt(i) >=65 && user.charCodeAt(i) <= 90)
      {
      event.preventDefault();
      $("#usuario").css("border", " 1px solid red");
      }
      var cantidadUsuario = i;
    }
    if (cantidadUsuario == 0) {
      event.preventDefault();
      $("#usuario").css("border", " 1px solid red");
    }
  });
});
