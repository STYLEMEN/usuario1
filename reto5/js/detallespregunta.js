$(document).ready(function()
{
  $('#botonRespuesta').click(function()
  {
    $('#botonRespuesta').hide();
    $('#bontonSinRespuesta').show();
    $('#explicacion').show();
    $('.0').hide();
    $('#respuestas').css("column-count", "1");

  });

  $('#bontonSinRespuesta').click(function()
  {
    $('#botonRespuesta').show();
    $('#bontonSinRespuesta').hide();
    $('#explicacion').hide();

    $('.0').show();
    $('#respuestas').css("column-count", "2");
  });
});
