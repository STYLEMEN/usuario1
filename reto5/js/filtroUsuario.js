$(document).ready(function()
{
  $('#filtro').on("change textInput input",function(){
    var textoFiltro = $("#filtro").val();

    $(".paleBlueRows td").each(function(index)
    {
      if(index % 4 == 0)
      {
        var texto = $(this).text();
        if(!texto.includes(textoFiltro))
        {
          $(this).parent('tr').hide();
        } else {
          $(this).parent('tr').show();
        }
      }
    });
  });

  $('#limpiarFiltro').click(function()
  {
    $("#filtro").val("");
    var textoFiltro = $("#filtro").val();

    $(".paleBlueRows td").each(function(index)
    {
      if(index % 4 == 0)
      {
        var texto = $(this).text();
        if(!texto.includes(textoFiltro))
        {
          $(this).parent('tr').hide();
        } else {
          $(this).parent('tr').show();
        }
      }
    });
  });



//FILTRO COMBOBOX--------------------------------------------------------

  $('.combobox').on("change", function(){
    var id_permiso = $(this).val();
    console.log(id_permiso);

    //$('#tabla').html("");

    var parametros = {function: 'usuariosbyIdPermiso', id_permiso: id_permiso};

    $.ajax(
      {
        data:parametros,
        url:"http://192.168.6.215/servicios.php",
        type:"post",
        success:function(response)
        {
          var usuariosArray = $.parseJSON(response);
           $('.hola').html("");
          for(var i = 0; i < usuariosArray.length; i++)
          {
            var usuarios = usuariosArray[i];

            console.log(usuarios.usuario);


             var htmlNoticia = "<tr class='hola'>";
            htmlNoticia +="<td>'"+usuarios.usuario+"'</td>";
            htmlNoticia +="<td>'"+usuarios.email+"'</td>";
            if(usuarios.admin == 3){
                htmlNoticia +="<td><a href='permisos.php?id_usuario="+usuarios.id_usuario+"'><img src='imagenes/moderador.png'/ class='imagenMod'></a></td>";
            }
            if(usuarios.admin == 2){
                htmlNoticia +="<td><a href='quitarPermisos.php?id_usuario="+usuarios.id_usuario+"'><img src='imagenes/eliminarmod.png'/ class='imagenMod'></a></td>";
            }
            htmlNoticia +="<td class='borrar'><a href='borrarUsuario.php?id_usuario="+usuarios.id_usuario+"'><img src='imagenes/eliminarUsuario.png'/ class='imagenBorrar'></a></td>";
            htmlNoticia +="</tr>";

            $('.paleBlueRows').append(htmlNoticia);

          }
          $('.borrar').click(function()
          {
            var mensaje = confirm("¿Quieres borrar este Usuario?");
            if (mensaje == true)
            {

            }
            else {
              return false;
            }
          });
        },
        error:function(err)
        {
          console.log(err);
        }
    });
  });

});
