<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Reto 5</title>
    <link rel="stylesheet" type="text/css" href="css/crearpreguntas.css">
    <link rel="stylesheet" type="text/css" href="css/comun.css">
    <?php
      session_start();
      include("datos.php");
    ?>
  </head>
  <body>
    <div id="encabezado">
      <h1>STYLEMEN</h1>
      <div id="menu">
        <ul>
          <li>
            <a href="index.php" class="enlaceInicio">Inicio</a>
          </li>
          <?php

          if (isset( $_SESSION["user"] ) == false){
          echo"<li>";
            echo"<a href='login.php'class='enclaceLogin'>Inicio sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='registro.php' class='enlaceRegistro'>Registar</a>";
          echo "</li>";
        }else {
          if( $_SESSION['admin'] == 1)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
            echo "<li>";
              echo"<a href='filtroUsuario.php'>Usuarios</a>";
            echo "</li>";
          }
          if( $_SESSION['admin'] == 2)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
          }
          echo"<li>";
            echo"<a href='crearpregunta.php'class='enclaceLogin'>Crear Pregunta</a>";
          echo"</li>";
          echo "<li><a href='validar.php'>Validar Preguntas</a></li>";
          echo"<li>";
            echo"<a href='cerrarSesion.php'class='enclaceLogin'>Cerrar Sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='usuario.php?id_usuario=".$_SESSION['id_user']."' class='enlaceRegistro'>".$_SESSION['user']."</a>";
          echo "</li>";


        }
          ?>
        </ul>
      </div>
    </div>
    <div id="cuerpo">
        <form id="formulario" action="meterImagen.php" method="post" enctype="multipart/form-data">
          <br>
          <label>Pregunta</label>
          <br>
          <input type='text' value="" id="pregunta">
          <div id="errorPregunta">
            <p>La pregunta ya Existe</p>
          </div>
          <br>
          <br>
          <label>Respuesta 1</label>
          <br>
          <input type='text' value="" id="respuesta1">
          <input type='checkbox'id='validacion1' class='validacion' value="">
          <br>
          <br>
          <label>Respuesta 2</label>
          <br>
          <input type='text' value="" id="respuesta2">
          <input type='checkbox'id='validacion2' class='validacion' value="">
          <br>
          <br>
          <label>Respuesta 3</label>
          <br>
          <input type='text' value="" id="respuesta3">
          <input type='checkbox'id='validacion3' class='validacion' value="">
          <br>
          <br>
          <label>Respuesta 4</label>
          <br>
          <input type='text' value="" id="respuesta4">
          <input type='checkbox'id='validacion4' class='validacion' value="">
          <br>
          <br>
          <label>Explicacion</label>
          <br>
          <input id='explicacion' type='text' value="" >
          <br>
          <div id= "respuestaCorrecta">
            <p>Seleccione la respuesta correcta</p>
          </div>
          <br>
          <label>Categoria</label>
          <select id="categoria">
          <option>FOL</option>
          <option>Lenguaje</option>
          <option>JAVA</option>
          <option>Ingles</option>
          </select>
          <label>Nivel</label>
          <select  id="nivel" >
          <option>bajo</option>
          <option>medio</option>
          <option>alto</option>
          </select>
          <label>Idioma</label>
          <select  id="idioma">
          <option>eng</option>
          <option>esp</option>
          </select>
        <label>Imagen</label>
          <input type="file"  id="imagen" name="imagen"/>
          <br>
          <div id="ErrorImagen">
            <p>Seleccione una imagen</p>
          </div>
          <br>
          <input id='crear' type='submit' value='Crear Pregunta'>
        </form>
      <br>
    </div>
    <div id="pie">
    </div>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/crearpregunta.js"></script>
  </body>
</html>
