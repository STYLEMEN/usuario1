<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Reto 5</title>
    <link rel="stylesheet" type="text/css" href="css/filtroUsuario.css">
    <link rel="stylesheet" type="text/css" href="css/comun.css">
    <?php
      session_start();
      include("datos.php");
    ?>
  </head>
  <body>
    <div id="encabezado">
      <h1>STYLEMEN</h1>
      <div id="menu">
        <ul>
          <li>
            <a href="index.php" class="enlaceInicio">Inicio</a>
          </li>
          <?php

          if (isset( $_SESSION["user"] ) == false){
          echo"<li>";
            echo"<a href='login.php'class='enclaceLogin'>Inicio sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='registro.php' class='enlaceRegistro'>Registar</a>";
          echo "</li>";
        }else {
          if( $_SESSION['admin'] == 1)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
            echo "<li>";
              echo"<a href='filtroUsuario.php'>Usuarios</a>";
            echo "</li>";
          }
          if( $_SESSION['admin'] == 2)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
          }
          echo"<li>";
            echo"<a href='crearpregunta.php'class='enclaceLogin'>Crear Pregunta</a>";
          echo"</li>";
          echo "<li><a href='validar.php'>Validar Preguntas</a></li>";
          echo"<li>";
            echo"<a href='cerrarSesion.php'class='enclaceLogin'>Cerrar Sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='usuario.php?id_usuario=".$_SESSION['id_user']."' class='enlaceRegistro'>".$_SESSION['user']."</a>";
          echo "</li>";


        }
          ?>
        </ul>
      </div>
    </div>
    <div id="cuerpo">
      <div id="input">
        <input type="text" id="filtro" value="">
        <input type="button"id="limpiarFiltro" value="Limpiar Filtro">
        <select class="combobox">
          <option class='opciones' value="0"></option>
          <?php
          $nombres = permisos();
          for($i = 1; $i < sizeof($nombres); $i++)
          {
            echo "<option class='opciones' value='".$nombres[$i]['id_permiso']."'>".$nombres[$i]['nombre']."</option>";
          }
           ?>
        </select>
      </div>
      <table class="paleBlueRows">
        <tr>
          <th>Usuario</th>
          <th>Correo Electronico</th>
          <th>Permisos</th>
          <th>Eliminar</th>
        </tr>
      </table>

    </div>
    <div id="pie">
    
    </div>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/filtroUsuario.js"></script>
  </body>
</html>
