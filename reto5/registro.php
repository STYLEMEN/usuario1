<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Reto 5</title>
    <link rel="stylesheet" type="text/css" href="css/registro.css">
    <link rel="stylesheet" type="text/css" href="css/comun.css">
    <?php
      session_start();
      include("datos.php");
    ?>
  </head>
  <body>
    <div id="encabezado">
      <h1>STYLEMEN</h1>
      <div id="menu">
        <ul>
          <li>
            <a href="index.php" class="enlaceInicio">Inicio</a>
          </li>
          <?php

          if (isset( $_SESSION["user"] ) == false){
          echo"<li>";
            echo"<a href='login.php'class='enclaceLogin'>Inicio sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='registro.php' class='enlaceRegistro'>Registar</a>";
          echo "</li>";
        }else {
          if( $_SESSION['admin'] == 1)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
            echo "<li>";
              echo"<a href='filtroUsuario.php'>Usuarios</a>";
            echo "</li>";
          }
          if( $_SESSION['admin'] == 2)
          {
            echo "<li><a href='mongoFOL.php'>Preguntas</a></li>";
          }
          echo"<li>";
            echo"<a href='crearpregunta.php'class='enclaceLogin'>Crear Pregunta</a>";
          echo"</li>";
          echo "<li><a href='validar.php'>Validar Preguntas</a></li>";
          echo"<li>";
            echo"<a href='cerrarSesion.php'class='enclaceLogin'>Cerrar Sesion</a>";
          echo"</li>";
          echo "<li>";
            echo"<a href='usuario.php?id_usuario=".$_SESSION['id_user']."' class='enlaceRegistro'>".$_SESSION['user']."</a>";
          echo "</li>";


        }
          ?>
        </ul>
      </div>
    </div>
    <div id="cuerpo">
      <form id="formularioRegistro" action="altaUsuario.php" method="post">
        <label>Nombre de usuario:</label>
        <input type="text" id="usuario" name="usuario" value="">
        <div id="resultado">
        </div>
        <br>
        <label>Contraseña:</label>
        <input id="contrasena" type="password" name="contrasena" value="">
        <input type="checkbox" id="mostrar_contrasena" title="clic para mostrar contraseña"/>
        <div id="mensajeContraseña">
        </div>
        <br>
        <div id="contasenaRepetir">
          <label>Repetir contraseña:</label>
          <input id="repetirContrasena" type="password" name="repetircontrasena" value="">
          <br>
        </div>
        <label>Correo electronico:</label>
        <input id="email" type="text" name="email" value="">
        <br>
        <div id="mensajeCorreo">
        </div>
        <input id="enviar" type="submit" name="enviar" value="Registrar">
      </form>
    </div>
    <div id="pie">
      <footer>

      </footer>
    </div>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/registro.js"></script>
  </body>
</html>
